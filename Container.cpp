﻿
#include "Container.h"
//инициализировать класс контейнера
Container::Container() {
	this->init();
}
//инициализировать класс контейнера и поставляет тип контейнера
Container::Container(std::string t){
	this->init();
	this->type = t;
}
//инициализировать класс контейнера, поставляет тип заголовок контейнера
Container::Container(std::string t, std::string type) {
	this->init();
	this->title = t;
	this->type = type;
}
//инициализировать контейнер, вызвав Component :: setDefault ()
void Container::init() {
	this->setDefault();
}
//проверяет, содержит ли контейнер какие-либо подэлементы
bool Container::hasChildren() {
	return this->children.size() || this->containerChildren.size();
}
//добавляет элемент типа компонента данного заголовок и тип элемента
void Container::addChild(std::string t, std::string c){
	Component newComponent(t, c);
	this->children.push_back(newComponent);
}
//добавляет элемент типа компонента с учетом существующего элемента
void Container::addChild(Component comp) {
	this->children.push_back(comp);
}
//добавляет элемент типа контейнера 
void Container::addChild(Container a) {
	Container * nu = new Container(a);
	this->containerChildren.push_back(nu);
}
//получить адрес к компоненту, чтобы сосредоточиться на нем и внести любые изменения, сделанные непосредственно на него
Component* Container::getComponent(std::string ttle) {
	for (auto &i : this->children)
		if(i.getTitle() == ttle)
			return &i;
	return new Component;
}
//получить адрес к контейнеру, чтобы сосредоточиться на нем и внести любые изменения, внесенные непосредственно в него
Container* Container::getContainer(std::string ttle) {
	for (auto i : this->containerChildren)
		if (i->getTitle() == ttle)
			return i;
	return new Container;
}
//распечатать детали контейнера
std::string Container::printDetails() {
	std::string s = "";
	s += this->Component::printDetails();
	for (auto &i : this->containerChildren)
		s += "\t\\____" + i->printDetails("\t\t");
	for (auto &i : this->children) 
		s += "\t\\____" + i.printDetails();
	
	return s+"\n";
}
//распечатайте детали контейнера и добавьте вкладки. обычно используется при рекурсивном обходе дерева
std::string Container::printDetails(std::string tabs) {
	std::string s = "";
	s += this->Component::printDetails();
	for(auto &i : this->containerChildren)
		s += tabs + "\\____" + i->printDetails(tabs+tabs);
	for (auto &i : this->children) 
		s += tabs + "\\____" + i.printDetails();

	return s+"\n";
}
//получить вектор типа и названия всех подэлементов в контейнере.
std::vector<Component> Container::listCildren() {
	std::vector<Component> s;
	for (auto &i : this->containerChildren) {
		s.push_back(Component(i->getTitle(), i->getType()));
	}
	for (auto &i : this->children) {
		s.push_back(i);
	}
	return s;
}
Container::~Container(){
}
