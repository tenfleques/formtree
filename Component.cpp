﻿
#include "Component.h"
//инициализировать класс <<компонент>>
Component::Component() {
	this->setDefault();
}
//инициализировать класс <<компонент>> и задает заголовок  из заданного значения
Component::Component(std::string text) {
	this->setDefault();
	this->title = text;
}
//инициализировать класс <<компонент>> и задает название и тип компонента из заданных значений
Component::Component(std::string t, std::string c) {
	this->setDefault();
	this->title = t;
	this->type = c;
}
//возвращает значение заголовка для общего доступа
std::string Component::getTitle() {
	return this->title;
}
//публично изменить значение заголовка
void Component::setTitle(std::string t) {
	this->title = t;
}
//публично установить тип компонента
void Component::setType(std::string t) {
	this->type = t;
}
//получить тип компонента
std::string Component::getType() {
	return this->type;
}
//устанавливает значения по умолчанию
void Component::setDefault() {
	this->title = "null";
	this->type = "bez tipa";
	this->visible = true;
}
//скрывает элемент
void Component::hide() {
	this->visible = false;
}
//показывает элемент
void Component::show() {
	this->visible = true;
}
//переключить видимость элемента
void Component::toggleView() {
	this->visible = !this->visible;
}
//переместите элемент из того места, где оно находится в позиции, заданной значениями x и y
Position Component::move(int x, int y) {
	this->position.setXY(x, y);
	return this->position;
}
//распечатывает атрибуты вкладок компонента и добавок, которые полезны при печати обхода дерева
std::string Component::printDetails(std::string tabs) {
	std::string s = "";
	s += tabs + "tip: " + this->type;
	s += " nazvanie: " + this->title;
	s += " vidimii: ";
	s += (this->visible) ? "da" : "net";
	s += " polozhenie: " + this->position.getXY() + "\n";	
	return s;
}
//печатает атрибуты элемента
std::string Component::printDetails() {
	std::string s = "";
	s += "tip: " + this->type;
	s += " nazvanie: " + this->title;
	s += " vidimii:";
	s += (this->visible) ? "da" : "net";
	s += " polozhenie: " + this->position.getXY() + "\n";
	return s;
}
Component::~Component(){
}
