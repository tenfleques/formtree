#include "Container.h"
#include <cstdlib>

void operateComponent(Component &component);
void operateContainer(Container &container);

void cls() {
#if defined(_WIN32)
	system("cls");

#else
	system("clear");
#endif
}

using namespace std;

char getChar_s(bool cl = false, string msg = "") {
	string input;
	char myChar = {0};
	while (true) {
		if (cl) {
			cls();
			cout << msg;
		}
		else if (msg.size()) {
			cout << msg;
		}
		getline(cin, input);
		if (input.length() == 1) {
			myChar = input[0];
			break;
		}
	}
	return myChar;
}

Component testButton(bool print = true) {
	Component btn;
	if (print)
		cout << btn.printDetails(); // print the initial status
	btn.move(2, 5); // move the button

	if (print) {
		cout << "\n\nmove 2 to the right and 5.6 downwards " << endl;
		cout << btn.printDetails(); //print out the new status 
		cout << "\n\nhide button " << endl;
	}
	btn.hide(); //hide the button
	if (print) {
		cout << btn.printDetails(); //print the new status
		cout << "\n\ntoggle visibility of button " << endl;
	}
	btn.toggleView(); //change the visibility of the button without knowing the previous status
	if (print)
		cout << btn.printDetails(); //print the new status
	return btn;
}

void testContainer() {
	Container form("main","window");
	Container panel("panel","panel");
	Component label("label"), line("line"), btn = testButton(0);
	panel.addChild(btn);
	panel.addChild(panel);
	panel.addChild(label);
	form.addChild(btn);
	form.addChild(panel);
	form.addChild(line);

	std::cout << form.printDetails();
	form.getContainer("panel")->move(5, 6);
	form.getContainer("panel")->getContainer("panel")->getComponent("label")->toggleView();
	form.getComponent("line")->hide();
	std::cout << form.printDetails();
}
Component createComponent(string type,string title) {
	string t;
	cout << "type the title of the " + type + "\n" + title + "/";
	getline(cin, t);
	return Component(title + "/" + t, type);
}
Container createContainer(string type,string title) {
	string t;
	cout << "type the title of the " + type + "\n" + title + "/";
	getline(cin, t);
	return Container(title + "/" + t, type);
}
void addComponent(Container &c) {
	int r = 1;
	string options[5] = {"panel","line","button","list","label"};

	string instructions = "";
	int j = 1;
	for(auto &i : options)
		instructions += "\n "+to_string(j++)+" : "+i;
	cout << instructions + "\n";
	r = getChar_s();
	r -= 49;

	if (r < 0 || r > 4)
		return;
	if (r == 0)
		c.addChild(createContainer("panel",c.getTitle()));
	else
		c.addChild(createComponent(options[r], c.getTitle()));
}
void selectOptions(Container &form) {
	int response;
	vector<Component> children = form.listCildren();
	string options = "";
	int j = 1;
	for (auto &i : children)
		options += "\n" + to_string(j++) +". "+ i.getType() + "=> " + i.getTitle();
	response = getChar_s(false, options + "\nchoose from the above list: \n");
	response -= 49;


	if (response > -1 && response < children.size()) {
		string choice = children[response].getTitle();
		if (children[response].getType() == "panel") { 
			operateContainer(*form.getContainer(choice));
		}
		else {//this is a non container
			operateComponent(*form.getComponent(choice));
		}
	}
	return;
}
void moveElement(Component &el) {
	cout << "\ntype value for translation in x: \n";
	int x = getChar_s() - 48;
	cout << "type value for translation in y: \n";
	int y = getChar_s() - 48;
	el.move(x,y);
}
void operateComponent(Component &component) {
	Component element;
	char response;
	bool exitFlag = false;
	string instructions = component.getTitle() + "\nExit: x \t Print p\t Move m \t Hide h \t Show s \t Toggle t\n";
	cout << instructions;
	while (true) {
		response = getChar_s();
		switch (response) {
		case 'x':
			exitFlag = true;
			break;
		case 'm':
			moveElement(component);
			cout <<
				component.printDetails();
			break;
		case 'h':
			component.hide();
			cout <<
				component.printDetails();
			break;
		case 's':
			component.show();
			cout <<
				component.printDetails();
			break;
		case 't':
			component.toggleView();
			cout <<
				component.printDetails();
			break;
		case 'p':
			cout <<
				component.printDetails();
			break;

		}
		if (exitFlag)
			break;
	}
}

void operateContainer(Container &container) {
	if (container.getTitle() == "null") {
		string title;
		cout << "type the title of the " + container.getType() + ": \n";
		getline(cin, title);
		container.setTitle(title);
	}	
	Component element;
	Container containerElement;
	string createNewResponse;
	char response;
	bool exitFlag = false;
	string instructions = container.getTitle() + "\nExit: x \t New: n \t Print p\n";
	while (true) {
		if(container.hasChildren())
			instructions = container.getTitle() + "\nExit: x \t New: n \t Print p \t Select: s\n";
		response = getChar_s(true, instructions);;
		switch (response) {
			case 'x':
				exitFlag = true;
				break;
			case 'n':
				addComponent(container);
				break;
			case 's':
				selectOptions(container);
				break;
			case 'p':
				cout << 
					container.printDetails();
				cout << "press enter to continue...";
				cin.ignore();
				break;

		}
		if (exitFlag)
			break;
	}
}
int main() {
	char response;
	string instructions = "Exit: x \t New Window: n \n";
	bool exitFlag = false;
	while(true){//start the infinite loop for user interaction
		Container container("window");
		response = getChar_s(true,instructions);
		switch(response){
			case 'n':
				operateContainer(container);
				break;
			case 'x':
				exitFlag = true;
				break;
		}
		if(exitFlag)
			break;
	}
	system("pause");
	return 0;
}

