﻿#include "Container.h"
#include <cstdlib>


void operateComponent(Component &component);
void operateContainer(Container &container);

/*
	эта функция очищает выходной экран на платформах Windows и Unix.
*/
void cls() {
#if defined(_WIN32)
	system("cls");

#else
	system("clear");
#endif
}

using namespace std;


/*эта функция безопасно получает символ от ввода. Аргументы, используемые для взаимодействия с пользователем. если cl установлен, то функция очищает вывод и печатает предоставленное сообщение, иначе передает сообщение, учитывая, что он имеет размер*/
char getChar_s(bool cl = false, string msg = "") {
	string input;
	char myChar = {0};
	while (true) { // повторите процесс до тех пор, пока пользователь не установит правильное значение и / или нормальное значение
		if (cl) {
			cls();
			cout << msg;
		}
		else if (msg.size()) {
			cout << msg;
		}
		getline(cin, input);
		if (input.length() == 1) {
			myChar = input[0];
			break;
		}
	}
	return myChar;
}
/*
	эта функция обеспечивает интерактивные процессы для создания компонента. Обязательные значения - это значения для типа компонента и родительского имени для добавления к имени компонента, чтобы создать абсолютный путь к компоненту в следующем порядке root/[parent/]component
*/
Component createComponent(string type,string title) {
	string t;
	cout << "vvedite nazvaniye " + type + ":\n" + title + "/";
	getline(cin, t);
	return Component(title + "/" + t, type);
}
/*
	эта функция обеспечивает интерактивный процесс создания типа контейнера. Необходимые аргументы, тип контейнера [панель или окно] и имя родительского контейнера для добавления к имени этого, чтобы обеспечить абсолютный путь к контейнеру следующим образом: root/[parent/]component
*/
Container createContainer(string type,string title) {
	string t;
	cout << "vvedite nazvaniye" + type + ":\n" + title + "/";
	getline(cin, t);
	return Container(title + "/" + t, type);
}
/*
	given the address to the container, this function asks the user to choose from the list 
*/
void addComponent(Container &c) {
	int r = 1;
	string options[5] = { "panel", "nadpis", "spisok", "knopka", "liniya" };

	string instructions = "";
	int j = 1;
	for(auto &i : options)
		instructions += "\n "+to_string(j++)+" : "+i;
	cout << instructions + "\n";
	r = getChar_s();
	r -= 49;

	if (r < 0 || r > 4)
		return;
	if (r == 0)
		c.addChild(createContainer("panel",c.getTitle()));
	else
		c.addChild(createComponent(options[r], c.getTitle()));
}
/*
	эта функция выбирает компонент для работы, перечисляя все элементы, содержащиеся в окне. Это позволяет перемещаться от родителя (root) до последнего листа на ветке
*/
void selectOptions(Container &form) {
	int response;
	vector<Component> children = form.listCildren();
	string options = "";
	int j = 1;
	for (auto &i : children)
		options += "\n" + to_string(j++) +". "+ i.getType() + "=> " + i.getTitle();
	response = getChar_s(false, options + "\nviberite iz privedenogo cpiska: \n");
	response -= 49;


	if (response > -1 && response < children.size()) {
		string choice = children[response].getTitle();
		if (children[response].getType() == "panel") { 
			Container *tmp = form.getContainer(choice);
			operateContainer(*tmp);
		}
		else {//this is a non container
			operateComponent(*form.getComponent(choice));
		}
	}
	return;
}
/*
	эта функция позволяет интерактивный процесс для получения координат места назначения, чтобы перемещать элемент в его контейнере.
*/
void moveElement(Component &el) {
	cout << "\nvvesti znacheniye x: \n";
	int x = getChar_s() - 48;
	cout << "vvesti znacheniye y: \n";
	int y = getChar_s() - 48;
	el.move(x,y);
}
/*
	эта функция обеспечивает взаимодействие с выбранным элементом. Она дает пользователю меню для перемещения, печати, скрытия, отображения, переключения и выхода из фокуса элемента. 
*/
void operateComponent(Component &component) {
	Component element;
	char response;
	bool exitFlag = false;
	string instructions = component.getTitle() + "\nVixhod: x \t Pechat: p\t Peremeshaite: m \t Skrivaite: h \t Pokazhet: s \t Pereklyichit: t\n";
	cout << instructions;
	while (true) {
		response = getChar_s();
		switch (response) {
		case 'x':
			exitFlag = true;
			break;
		case 'm':
			moveElement(component);
			cout <<
				component.printDetails();
			break;
		case 'h':
			component.hide();
			cout <<
				component.printDetails();
			break;
		case 's':
			component.show();
			cout <<
				component.printDetails();
			break;
		case 't':
			component.toggleView();
			cout <<
				component.printDetails();
			break;
		case 'p':
			cout <<
				component.printDetails();
			break;

		}
		if (exitFlag)
			break;
	}
}
/*
	эта функция обеспечивает взаимодействие с выбранным контейнером. Она дает пользователю меню для создания подэлемента, печати, выбора подэлемента и выхода из фокуса контейнера.
*/
void operateContainer(Container &container) {
	if (container.getTitle() == "null") {
		string title;
		cout << "vvedite nazvanie " + container.getType() + ": \n";
		getline(cin, title);
		container.setTitle(title);
	}	
	Component element;
	Container containerElement;
	string createNewResponse;
	char response;
	bool exitFlag = false;
	string instructions = container.getTitle() + "\nVixhod: x \t Novoe: n \t Pechat: p\n";
	while (true) {
		if(container.hasChildren())
			instructions = container.getTitle() + "\nVixhod: x \t Novi: n \t Pechat: p \t Vibiraite: s\n";
		response = getChar_s(true, instructions);;
		switch (response) {
			case 'x':
				exitFlag = true;
				break;
			case 'n':
				addComponent(container);
				break;
			case 's':
				selectOptions(container);
				break;
			case 'p':
				cout << 
					container.printDetails();
				cout << "nazhmite lyuboi klavish...";
				cin.ignore();
				break;

		}
		if (exitFlag)
			break;
	}
}
int main() {
	char response;
	string instructions = "vixhod: x \t novoe okno: n \n";
	bool exitFlag = false;
	while(true){//start the infinite loop for user interaction
		Container container("okno");
		response = getChar_s(true,instructions);
		switch(response){
			case 'n':
				operateContainer(container);
				break;
			case 'x':
				exitFlag = true;
				break;
		}
		if(exitFlag)
			break;
	}
	system("pause");
	return 0;
}

