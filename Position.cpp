﻿
#include "Position.h"
//инициализировать объект, установив значения по умолчанию
Position::Position(){
	this->x = 0;
	this->y = 0;
}
//инициализировать объект, назначая заданные значения для x и y
Position::Position(int x, int y) : y(y), x(x) {};
//устанавливает значения x y
void Position::setXY(int argx, int argy) {
	this->y = argy;
	this->x = argx;
};
//устанавливает значение x
void Position::setX(int argx) {
	this->x = argx;
};
//устанавливает значение y
void Position::setY(int argy) {
	this->y = argy;
};
//получить координату x
int Position::getX() {
	return this->x;
}
//получить координату y
int Position::getY() {
	return this->y;
}
//получить координаты
std::string Position::getXY() {
	return "(" + std::to_string(x) + ";" + std::to_string(y) + ")";
}

Position::~Position(){
}
