#pragma once
#include <string>
class Position{
protected:
	int x, y;
public:
	Position();
	Position(int x, int y);
	void setXY(int argx, int argy);
	void setX(int argx);
	void setY(int argy);
	int getX();
	int getY();
	std::string getXY();
	~Position();
};

