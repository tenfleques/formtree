#pragma once
#include "Component.h"
#include <list>
#include <vector>

class Container :
	public Component{
private:
	std::list<Component> children;
	std::list<Container *> containerChildren;
public:
	void addChild(std::string,std::string); //require title and content to create child
	void addChild(Component); //ready made component 
	void addChild(Container); //ready made container 
	Component* getComponent(std::string); //access child type component for modification visibility or position
	Container* getContainer(std::string); //access child type container for modification visibility or position
	std::string printDetails(std::string);//gets a tab prepend
	std::string printDetails();
	std::vector<Component> listCildren();
	bool hasChildren();
	Container();
	Container(std::string); //initialize with type
	Container(std::string t, std::string type);
	void init();
	~Container();
};

