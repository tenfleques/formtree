#pragma once
#include "Position.h"
#include <iostream>

class Component{
protected:
	std::string title,type;
	Position position;
	bool visible = true;
public:
	Component();
	Component(std::string); //initialize with title
	Component(std::string, std::string); //initialize with title and type
	std::string getTitle();
	void setTitle(std::string);
	void setType(std::string);
	std::string getType();
	void setDefault();//initialize all properties
	void hide(); // hides the component
	void show(); // shows the component
	void toggleView(); // if visible sets component invisible, visible otherwise
	Position move(int, int);//moves the component
	std::string printDetails(std::string); //print details of component inclusive of input tabs
	std::string printDetails(); //print details of component
	~Component();
};

